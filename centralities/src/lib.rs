use std::{
    collections::VecDeque,
    num::NonZeroUsize,
    sync::{
        mpsc::{self, Sender},
        Arc,
    },
    thread,
};

use indicatif::{ProgressBar, ProgressStyle};
use pyo3::prelude::*;

const DISCONNECTED_DEFAULT: NonZeroUsize = unsafe { NonZeroUsize::new_unchecked(50) };

#[derive(Clone, Debug)]
struct Worker {
    n_vertices: usize,
    worker_id: usize,
    n_workers: usize,
    sender: Sender<()>,
    graph: Arc<Vec<Vec<usize>>>,
    stack: Vec<usize>,
    predecessors: Vec<Vec<usize>>,
    path_counts: Vec<usize>,
    distances: Vec<Option<NonZeroUsize>>,
    queue: VecDeque<usize>,
    delta: Vec<f64>,
    betweenness_centrality: Vec<f64>,
    closeness_centrality: Vec<f64>,
}

impl Worker {
    fn new(
        worker_id: usize,
        n_workers: usize,
        sender: Sender<()>,
        graph: Arc<Vec<Vec<usize>>>,
    ) -> Self {
        let n_vertices = graph.len();

        let mut predecessors: Vec<Vec<usize>> = Vec::with_capacity(n_vertices);
        for neighborhood in graph.iter() {
            predecessors.push(Vec::with_capacity(neighborhood.len()));
        }

        Self {
            n_vertices,
            worker_id,
            n_workers,
            sender,
            graph,
            stack: Vec::with_capacity(n_vertices),
            predecessors,
            path_counts: vec![0; n_vertices],
            distances: vec![None; n_vertices],
            queue: VecDeque::with_capacity(n_vertices),
            delta: vec![0.0; n_vertices],
            betweenness_centrality: vec![0.0; n_vertices],
            closeness_centrality: vec![0.0; n_vertices],
        }
    }

    unsafe fn run(mut self) -> (Vec<f64>, Vec<f64>) {
        let mut vertex = self.worker_id;

        loop {
            self.single_source_shortest_paths(vertex);

            self.accumulate_betweenness(vertex);
            self.accumulate_closeness(vertex);

            vertex += self.n_workers;
            self.sender.send(()).unwrap();

            if vertex >= self.n_vertices {
                break;
            }

            self.reset()
        }

        (self.betweenness_centrality, self.closeness_centrality)
    }

    fn reset(&mut self) {
        self.stack.clear();
        self.predecessors.iter_mut().for_each(|vec| vec.clear());
        self.path_counts.iter_mut().for_each(|c| *c = 0);
        self.distances.iter_mut().for_each(|d| *d = None);
        self.queue.clear();
        self.delta.iter_mut().for_each(|d| *d = 0.0);
    }

    unsafe fn single_source_shortest_paths(&mut self, vertex: usize) {
        self.path_counts[vertex] = 1;
        self.distances[vertex] = Some(NonZeroUsize::new(1).unwrap());
        self.queue.push_back(vertex);

        while let Some(vertex) = self.queue.pop_front() {
            self.stack.push(vertex);
            let next_distance = self
                .distances
                .get_unchecked(vertex)
                .unwrap_unchecked()
                .checked_add(1)
                .unwrap_unchecked();
            let n_paths = *self.path_counts.get_unchecked(vertex);

            for &neighbor in self.graph.get_unchecked(vertex) {
                match *self.distances.get_unchecked(neighbor) {
                    None => {
                        self.queue.push_back(neighbor);
                        *self.distances.get_unchecked_mut(neighbor) = Some(next_distance);
                        *self.path_counts.get_unchecked_mut(neighbor) += n_paths;
                        self.predecessors.get_unchecked_mut(neighbor).push(vertex);
                    }
                    Some(distance) if distance == next_distance => {
                        *self.path_counts.get_unchecked_mut(neighbor) += n_paths;
                        self.predecessors.get_unchecked_mut(neighbor).push(vertex);
                    }
                    _ => (),
                }
            }
        }
    }

    unsafe fn accumulate_betweenness(&mut self, src: usize) {
        while let Some(vertex) = self.stack.pop() {
            let coeff = (1. + *self.delta.get_unchecked(vertex))
                / *self.path_counts.get_unchecked(vertex) as f64;

            for &pred in self.predecessors.get_unchecked(vertex) {
                *self.delta.get_unchecked_mut(pred) +=
                    *self.path_counts.get_unchecked(pred) as f64 * coeff;
            }

            if vertex != src {
                *self.betweenness_centrality.get_unchecked_mut(vertex) +=
                    *self.delta.get_unchecked(vertex);
            }
        }
    }

    unsafe fn accumulate_closeness(&mut self, vertex: usize) {
        let sum_distances = self
            .distances
            .iter()
            .map(|d| d.unwrap_or(DISCONNECTED_DEFAULT).get())
            .sum::<usize>();

        self.closeness_centrality[vertex] = 1.0 / (sum_distances - self.n_vertices) as f64;
    }
}

fn double_vec_add(
    (mut lhs_1, mut lhs_2): (Vec<f64>, Vec<f64>),
    (rhs_1, rhs_2): (Vec<f64>, Vec<f64>),
) -> (Vec<f64>, Vec<f64>) {
    lhs_1.iter_mut().zip(rhs_1).for_each(|(l, r)| *l += r);
    lhs_2.iter_mut().zip(rhs_2).for_each(|(l, r)| *l += r);

    (lhs_1, lhs_2)
}

fn rescale(
    betweenness_centrality: &mut [f64],
    closeness_centrality: &mut [f64],
    n_vertices: usize,
) {
    let scale = if n_vertices > 2 {
        1.0 / ((n_vertices - 1) * (n_vertices - 2)) as f64
    } else {
        1.0
    };
    betweenness_centrality.iter_mut().for_each(|x| *x *= scale);

    closeness_centrality
        .iter_mut()
        .for_each(|x| *x *= (n_vertices - 1) as f64);
}

#[pyfunction]
unsafe fn betweenness_closeness_centralities(
    mut graph: Vec<Vec<usize>>,
    n_workers: usize,
) -> (Vec<f64>, Vec<f64>) {
    graph
        .iter_mut()
        .for_each(|neighbors| neighbors.sort_unstable());

    let pb = ProgressBar::new(graph.len() as u64);
    let style = ProgressStyle::default_bar().template(
        "[{elapsed_precise} > {eta_precise}] {bar:50.cyan/blue} {percent}% ({pos}/{len}) {per_sec}]",
    );
    pb.set_style(style.unwrap());

    let mut handles = Vec::with_capacity(n_workers);
    let graph = Arc::new(graph);
    let (sender, receiver) = mpsc::channel();

    for worker_id in 0..n_workers {
        let worker = Worker::new(worker_id, n_workers, sender.clone(), Arc::clone(&graph));
        let handle = thread::spawn(move || unsafe { worker.run() });
        handles.push(handle);
    }

    for _ in 0..graph.len() {
        receiver.recv().unwrap();
        pb.inc(1);
    }
    pb.finish();

    let (mut betweenness_centrality, mut closeness_centrality) = handles
        .into_iter()
        .map(|handle| handle.join().unwrap())
        .reduce(double_vec_add)
        .unwrap();

    rescale(
        &mut betweenness_centrality,
        &mut closeness_centrality,
        graph.len(),
    );

    (betweenness_centrality, closeness_centrality)
}

#[pymodule]
fn centralities(m: &Bound<'_, PyModule>) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(betweenness_closeness_centralities, m)?)?;

    Ok(())
}
