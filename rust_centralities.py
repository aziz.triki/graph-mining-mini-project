import math
import sys

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import pandas as pd
import scipy.stats as stats

import centralities


def main():
    n_workers = int(sys.argv[1])

    print("Building graph")

    df = pd.read_csv("large_twitch_features.csv")
    G = nx.Graph()

    for index, row in df.iterrows():
        G.add_node(
            row["numeric_id"],
            views=row["views"],
            mature=row["mature"],
            life_time=row["life_time"],
            created_at=row["created_at"],
            updated_at=row["updated_at"],
            dead_account=row["dead_account"],
            language=row["language"],
            affiliate=row["affiliate"],
        )

    df_edges = pd.read_csv("large_twitch_edges.csv")

    for index, row in df_edges.iterrows():
        G.add_edge(row["numeric_id_1"], row["numeric_id_2"])

    nodes = set(range(G.number_of_nodes()))

    for node in list(G.nodes()):
        if G.nodes[node]["views"] < 1e4:
            G.remove_node(node)
            nodes.remove(node)

    nodes = sorted(nodes)
    reverse_mapping = {x: i for i, x in enumerate(nodes)}

    graph = [[reverse_mapping[w] for w in G[v]] for v in nodes]

    print("Computing centralities")

    a, b = centralities.betweenness_closeness_centralities(graph, n_workers)
    a = np.array(a)
    b = np.array(b)
    nodes = np.array(nodes, dtype=np.int64)

    np.save("precomputed/betweenness", a)
    np.save("precomputed/closeness", b)
    np.save("precomputed/nodes", nodes)


if __name__ == "__main__":
    main()
