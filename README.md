# Graph Mining Mini-Project

Notre travail comporte 4 parties:
1. Dataset
2. Analyse de centralité
3. Détection de communautés
4. Apprentissage

Le repo contient un dossier pour chaque partie comportant un notebook. Nous avons sauvegardé et fournis les résultats de 
longs traitements pour faciliter l'éxecution du code. Lorsqu'une partie peut être sautée pour loader directement les données, ça sera précisé sur la cellule.



## Données utilisées

https://snap.stanford.edu/data/twitch_gamers.html



## Installation

### Python et packages

Nous avons testé le code sous Python 3.10 (normalement toute version >= 3.8 fonctionne). Les packages utilisés sont les suivants.
`numpy scipy matplotlib networkx pandas scikit-learn seaborn gensim node2vec`

### Centralités Closeness et Betweenness

L'implémentation présente dans NetworkX pour ces deux centralités est trop lente (environ deux jours pour le graphe réduit, et 
de l'ordre du mois pour le graphe complet). Nous avons donc ré-implémenté ces deux algorithmes  en Rust (langage compilé 
similaire à C++) de manière parallélisée (répertoire centralities). Étant donné que ce n'est pas une installation standard,
les centralités ont été pré-calculées dans le dossier precomputed. Si vous souhaitez exécuter le code vous même, voici les étapes.

- Installation de Rust : https://www.rust-lang.org/tools/install
- Installation de maturin (permet de créer un package Python écrit en Rust) : https://www.maturin.rs/installation
- Compilation et installation du package Python dans l'environnement Python actuel : `cd centralities; maturin develop --release`
- Calcul des centralités : `python rust_centralities.py n_workers` en remplaçant n_workers par le nombre de workers souhaités.
